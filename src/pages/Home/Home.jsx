import Input from "../../components/Input/Input";
import VerticalList from "../../components/VerticalList/VerticalList";
import { useCallback, useEffect, useState } from "react";
import useStyles from "./Home.style";
import { useSearchParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

function buildUrl(value) {
  return value.length > 0
    ? `${process.env.REACT_APP_API_URL}/search/movie?query=${value}&api_key=${process.env.REACT_APP_API_KEY}`
    : `${process.env.REACT_APP_API_URL}/movie/popular?api_key=${process.env.REACT_APP_API_KEY}`;
}

function Home() {
  const [params, setSearchParams] = useSearchParams();
  const [value, setValue] = useState(params.get("q") ?? "");
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({ type: "FETCH_MOVIES" });
  }, [dispatch]);

  const movies = useSelector((state) => state.movies);

  const onChange = useCallback(
    (e) => {
      setValue(e.target.value);

      setSearchParams(e.target.value ? { q: e.target.value } : "");
    },
    [setSearchParams]
  );

  const classes = useStyles();

  console.log(movies);
  return (
    <div className={classes.root}>
      <Input value={value} onChange={onChange} />
      <VerticalList className={classes.list} movies={movies} />
    </div>
  );
}

export default Home;
