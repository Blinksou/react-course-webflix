import { useNavigate, useParams } from "react-router-dom";
import { getImageMedia, getMovieFromId } from "../../utils/movie-helper";
import Rating from "../../components/Rating/Rating";
import BackButton from "../../components/BackButton/BackButton";
import Genre from "../../components/Genre/Genre";
import useStyles from "./Details.style";
import { useEffect, useState } from "react";
import HorizontalList from "../../components/HorizontalList/HorizontalList";
import FavoriteButton from "../../components/FavoriteButton/FavoriteButton";

const OVERVIEW_SIZE = 120;

function Details() {
  const params = useParams();
  const classes = useStyles();
  const navigate = useNavigate();
  const [movie, setMovie] = useState(null);
  const [expand, setExpand] = useState(false);

  const toggle = () => {
    setExpand(!expand);
  };

  // Utilisé quand on clique sur un film suggéré, sinon ça reload pas la page
  useEffect(() => {
    async function fetchData() {
      const response = await getMovieFromId(params.id);
      setMovie(response);
    }
    fetchData().catch(() => navigate("/", { replace: true }));
  }, [navigate, params]);

  return (
    // Apparemment nécessaire pour que React ne throw pas une erreur avant de rediriger
    movie ? (
      <div className={classes.root}>
        <BackButton />

        <section className={classes.movieCard}>
          <img src={getImageMedia(movie.poster_path, "w300")} alt={"poster"} />

          <div className={classes.movieDesc}>
            <h2>
              <span role={"img"} aria-label={"movie"}>
                🎞️
              </span>{" "}
              {movie.title} <FavoriteButton id={movie.id} />
            </h2>{" "}
            <p>
              Sorti le{" "}
              {new Date(movie.release_date).toLocaleDateString("fr-FR")}
            </p>
            {movie.genres.map((genre) => (
              <Genre
                className={classes.chip}
                key={genre.id}
                name={genre.name}
              />
            ))}
          </div>
        </section>
        <section>
          <p>
            {movie.overview.substring(
              0,
              expand ? movie.overview.length - 1 : OVERVIEW_SIZE
            )}
            {!expand && movie.overview.length > OVERVIEW_SIZE ? "..." : ""}
          </p>

          {!expand && (
            <>
              <br />
              <button onClick={toggle}>Lire plus</button>
            </>
          )}
        </section>
        <Rating note={movie.vote_average} />
        <h2>Contenu similaire</h2>
        <HorizontalList currentId={movie.id} genres={movie.genres} />
      </div>
    ) : null
  );
}

export default Details;
