import { createUseStyles } from "react-jss";
import { BREAKPOINT_SM } from "../../utils/breakpoints";

const useStyles = createUseStyles({
  root: {
    padding: 20,
  },
  movieCard: {
    display: "flex",
    flexWrap: "wrap",
  },
  movieDesc: {},
  [`@media screen and (min-width: ${BREAKPOINT_SM})`]: {
    movieDesc: {
      marginLeft: 20,
    },
  },
  chip: {
    marginRight: 3,
  },
});

export default useStyles;
