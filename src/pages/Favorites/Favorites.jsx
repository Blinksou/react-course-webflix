import useStyles from "./Favorites.style";

function Favorites() {
  const classes = useStyles();

  return <div className={classes.root} data-testid="Favorites" />;
}

export default Favorites;
