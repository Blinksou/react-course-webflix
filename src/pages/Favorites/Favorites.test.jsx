import React from "react";
import { render, screen } from "@testing-library/react";
import Favorites from "./Favorites";

describe(Favorites.name, () => {
  it("renders component successfully", () => {
    render(<Favorites />);
    const element = screen.getByTestId("Favorites");
    expect(element).toBeInTheDocument();
  });
});
