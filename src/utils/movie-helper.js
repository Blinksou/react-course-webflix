const API_URL = process.env.REACT_APP_API_URL;
const API_KEY = process.env.REACT_APP_API_KEY;

export async function getPopularMovies() {
  const response = await fetch(`${API_URL}/movie/popular?api_key=${API_KEY}`);

  if (!response.ok) {
    throw response;
  }

  return await response.json();
}

export async function searchMovieByQuery(query) {
  const response = await fetch(
    `${API_URL}/search/movie?query=${query}&api_key=${API_KEY}`
  );

  if (!response.ok) {
    throw response;
  }

  return await response.json();
}

export async function getMovieFromId(id) {
  const response = await fetch(`${API_URL}/movie/${id}?api_key=${API_KEY}`);

  if (!response.ok) {
    throw response;
  }

  return await response.json();
}

export function getImageMedia(path, sizing) {
  return `https://image.tmdb.org/t/p/${sizing ?? "w92"}/${path}`;
}
