export const BREAKPOINT_SM = "600px";
export const BREAKPOINT_MD = "900px";
export const BREAKPOINT_LG = "1200px";
export const BREAKPOINT_XL = "1536px";
