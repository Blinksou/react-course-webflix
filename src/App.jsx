import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./pages/Home/Home";
import Details from "./pages/Details/Details";
import Header from "./components/Layout/Header/Header";
import Favorites from "./pages/Favorites/Favorites";

function App() {
  return (
    <BrowserRouter>
      <Header />
      <main>
        <Routes>
          <Route path={"/"} element={<Home />} />
          <Route path={"/details/:id"} element={<Details />} />
          <Route path={"/favorites"} element={<Favorites />} />
        </Routes>
      </main>
    </BrowserRouter>
  );
}

export default App;
