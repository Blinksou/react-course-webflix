import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  root: {
    listStyle: "none",
    flexWrap: "wrap",
    display: "flex",
    margin: 0,
    padding: 0,
    alignItems: "center",
  },
  item: {
    padding: 8,
  },
});

export default useStyles;
