import Card from "../Card/Card";
import { Link } from "react-router-dom";
import useStyles from "./HorizontalList.style";
import { getPopularMovies } from "../../utils/movie-helper";
import { useEffect, useState } from "react";

function HorizontalList({ currentId, genres }) {
  const [movies, setMovies] = useState([]);
  const [suggestedMovies, setSuggestedMovies] = useState([]);

  useEffect(() => {
    async function fetchData() {
      const data = await getPopularMovies();

      setMovies(data.results);
    }

    fetchData();
  }, []);

  useEffect(() => {
    let suggestedMovies = movies.filter((movie) => {
      return (
        currentId !== movie.id &&
        genres &&
        genres.some((genre) => movie?.genre_ids?.includes(genre.id))
      );
    });

    setSuggestedMovies(
      suggestedMovies.sort(() => (Math.random() > 0.5 ? 1 : -1)).slice(0, 4)
    );
  }, [currentId, genres, movies]);

  const classes = useStyles();

  if (!suggestedMovies) {
    return null;
  }

  return (
    <ul data-testid={"HorizontalList"} className={classes.root}>
      {suggestedMovies.map((movie) => (
        <li className={classes.item} key={movie.id}>
          <Link to={`/details/${movie.id}`}>
            <Card movie={movie} poster={movie.poster_path} />
          </Link>
        </li>
      ))}
    </ul>
  );
}

export default HorizontalList;
