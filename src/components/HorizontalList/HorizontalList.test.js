import { render, screen } from "@testing-library/react";
import HorizontalList from "./HorizontalList";

describe(HorizontalList.name, () => {
  it("renders component successfully", () => {
    render(<HorizontalList />);
    const element = screen.getByTestId("HorizontalList");
    expect(element).toBeInTheDocument();
  });
});
