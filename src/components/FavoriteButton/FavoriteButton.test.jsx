import React from "react";
import { render, screen } from "@testing-library/react";
import FavoriteButton from "./FavoriteButton";

describe(FavoriteButton.name, () => {
  it("renders component successfully", () => {
    render(<FavoriteButton />);
    const element = screen.getByTestId("FavoriteButton");
    expect(element).toBeInTheDocument();
  });
});
