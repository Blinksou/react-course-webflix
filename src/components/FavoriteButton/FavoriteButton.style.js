import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  root: {
    background: "none",
    cursor: "pointer",
    color: "inherit",
    border: "none",
    outline: "inherit",
  },
  inactive: {
    filter: "grayscale(1)",
  },
  active: {
    opacity: 1,
  },
});

export default useStyles;
