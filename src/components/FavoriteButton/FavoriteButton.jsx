import useStyles from "./FavoriteButton.style";
import { useDispatch, useSelector } from "react-redux";
import { favorites as favoritesSlice } from "../../slices";
import classNames from "classnames";

function FavoriteButton({ id }) {
  const classes = useStyles();

  const favorites = useSelector((state) => state.favorites);
  const dispatch = useDispatch();
  const addToFavorite = (e) => {
    e.preventDefault();
    dispatch(favoritesSlice.actions.toggle({ id }));
  };

  return (
    <button
      onClick={addToFavorite}
      className={classNames(
        classes.root,
        Array.isArray(favorites) && favorites.includes(id)
          ? classes.active
          : classes.inactive
      )}
      data-testid="FavoriteButton"
    >
      <span role={"img"} aria-label={"heart"}>
        ❤️
      </span>
    </button>
  );
}

export default FavoriteButton;
