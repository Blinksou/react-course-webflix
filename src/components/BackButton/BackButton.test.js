import { render, screen } from "@testing-library/react";
import BackButton from "./BackButton";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "../../pages/Home/Home";

describe(BackButton.name, () => {
  it("renders component successfully", () => {
    render(
      <>
        <BrowserRouter>
          <Routes>
            <Route path={"/"} element={<Home />} />
          </Routes>

          <BackButton />
        </BrowserRouter>
      </>
    );

    expect(screen.getByTestId("backButton")).toBeInTheDocument();
  });
});
