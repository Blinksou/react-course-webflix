import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  root: {
    color: "black",
    display: "flex",
    justifyContent: "flex-end",
    fontSize: 16,
    paddingLeft: 10,
    paddingRight: 10,
  },
  button: {
    outline: "inherit",
    border: "1px solid #835ad2",
    backgroundColor: "#9575cd",
    padding: 14,
    cursor: "pointer",
  },
});

export default useStyles;
