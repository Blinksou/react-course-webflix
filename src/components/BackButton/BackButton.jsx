import { useNavigate } from "react-router-dom";
import useStyles from "./BackButton.style";

function BackButton() {
  const classes = useStyles();

  const navigate = useNavigate();

  const onClick = () => {
    navigate(-1);
  };

  return (
    <div className={classes.root}>
      <button
        data-testid={"backButton"}
        className={classes.button}
        onClick={onClick}
      >
        Retour
      </button>
    </div>
  );
}

export default BackButton;
