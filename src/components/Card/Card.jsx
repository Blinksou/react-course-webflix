import { getImageMedia } from "../../utils/movie-helper";
import useStyles from "./Card.style";
import FavoriteButton from "../FavoriteButton/FavoriteButton";

function Card({ poster, movie }) {
  const imageURL = poster ? getImageMedia(poster) : "";

  const classes = useStyles({
    imageURL,
  });

  return (
    <div data-testid={"Card"} className={classes.root}>
      <h3 className={classes.title}>{movie.title}</h3>
      <div className={classes.actions}>
        <FavoriteButton id={movie.id} />
      </div>
    </div>
  );
}

export default Card;
