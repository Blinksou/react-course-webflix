import { render, screen } from "@testing-library/react";
import Card from "./Card";

describe(Card.name, () => {
  it("renders component successfully", () => {
    render(<Card />);

    const element = screen.getByTestId("Card");
    expect(element).toBeInTheDocument();
  });
});
