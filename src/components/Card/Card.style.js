import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  root: {
    position: "relative",
    borderColor: "black",
    borderWidth: 2,
    borderStyle: "solid",
    height: 138,
    width: 92,
    background: ({ imageURL }) => (imageURL ? `url(${imageURL})` : "grey"),
    padding: 8,
    wordBreak: "break-word",
  },
  title: {
    color: "white",
    fontSize: 16,
    textShadow: "rgb(0, 0, 0, .8) 4px 4px 6px",
  },
  actions: {
    position: "absolute",
    bottom: -1,
    left: 0,
    right: 0,
    background: "rgba(0, 0, 0, 0.5)",
    paddingTop: 1,
    paddingBottom: 1,
  },
});

export default useStyles;
