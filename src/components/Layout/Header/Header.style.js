import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  root: {
    paddingRight: 20,
    paddingLeft: 20,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
});

export default useStyles;
