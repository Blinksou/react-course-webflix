import { Link } from "react-router-dom";
import useStyles from "./Header.style";
import { useSelector } from "react-redux";

function Header() {
  const classes = useStyles();

  const favorites = useSelector((state) => state.favorites);

  return (
    <header className={classes.root}>
      <h1>
        <span role={"img"} aria-label={"popcorn"}>
          🍿
        </span>{" "}
        Webflix
      </h1>
      <nav>
        <Link to="/">Home</Link>
        <Link to="/favorites">Favoris ({favorites.length ?? 0})</Link>
      </nav>
    </header>
  );
}

export default Header;
