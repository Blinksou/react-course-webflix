import { render, screen } from "@testing-library/react";
import Genre from "./Genre";

describe(Genre.name, () => {
  it("renders nothing without `id`", () => {
    render(<Genre />);

    expect(screen.queryByTestId("genre")).not.toBeInTheDocument();
  });
  it("renders something with `id`", () => {
    render(<Genre id={28} />);

    expect(screen.getByTestId("genre")).toBeInTheDocument();
  });
  it("renders `Action` with id 28", () => {
    render(<Genre id={28} />);

    expect(screen.getByText("Action")).toBeInTheDocument();
  });
});
