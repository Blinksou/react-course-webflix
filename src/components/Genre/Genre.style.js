import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  root: {
    padding: "5px 8px",
    backgroundColor: "black",
    color: "white",
    display: "inline-block",
    fontSize: 10,
  },
});

export default useStyles;
