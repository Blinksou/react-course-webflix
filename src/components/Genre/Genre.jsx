import useStyles from "./Genre.style";
import classNames from "classnames";

function Genre({ className, name }) {
  const classes = useStyles();

  return (
    <div
      data-testid={"genre"}
      className={classNames([className, classes.root])}
    >
      {name}
    </div>
  );
}

export default Genre;
