import useStyles from "./Input.style";

function Input({ value, onChange }) {
  const classes = useStyles();

  return (
    <input
      data-testid={"Input"}
      className={classes.root}
      value={value}
      onChange={onChange}
    />
  );
}

export default Input;
