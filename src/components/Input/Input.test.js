import { render, screen } from "@testing-library/react";
import Input from "./Input";

describe(Input.name, () => {
  it("renders component successfully", () => {
    render(<Input />);
    const element = screen.getByTestId("Input");
    expect(element).toBeInTheDocument();
  });
});
