import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  root: {
    borderColor: "black",
    borderWidth: 2,
    height: 32,
    width: "80%",
    margin: "auto",
    display: "flex",
    fontSize: 20,
    fontWeight: 700,
    "&focus": {
      outline: "none",
    },
  },
});

export default useStyles;
