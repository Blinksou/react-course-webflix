import Card from "../Card/Card";
import { Link } from "react-router-dom";
import useStyles from "./VerticalList.style";
import classNames from "classnames";

function VerticalList({ className, movies }) {
  const classes = useStyles();

  return (
    <ul
      data-testid={"VerticalList"}
      className={classNames([classes.root, className])}
    >
      {movies.map((movie) => (
        <li className={classes.item} key={movie.id}>
          <Link className={classes.link} to={`/details/${movie.id}`}>
            <Card movie={movie} poster={movie.poster_path} />
          </Link>
        </li>
      ))}
    </ul>
  );
}

export default VerticalList;
