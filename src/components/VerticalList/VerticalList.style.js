import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  root: {
    listStyle: "none",
    flexWrap: "wrap",
    display: "flex",
    margin: 0,
    padding: 0,
    justifyContent: "center",
    alignItems: "center",
  },
  item: {
    padding: 8,
  },
  link: {
    textDecoration: "none",
  },
});

export default useStyles;
