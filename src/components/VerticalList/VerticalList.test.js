import { render, screen } from "@testing-library/react";
import VerticalList from "./VerticalList";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Header from "../Layout/Header/Header";
import Details from "../../pages/Details/Details";
import Home from "../../pages/Home/Home";

describe(VerticalList.name, () => {
  it("renders component successfully", () => {
    render(
      <BrowserRouter>
        <Header />
        <main>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/details/:id" element={<Details />} />
          </Routes>
        </main>
        {/*<VerticalList movies={data.movies.slice(0, 1)} />*/}
      </BrowserRouter>
    );

    const element = screen.getByTestId("VerticalList");
    expect(element).toBeInTheDocument();
  });
});
