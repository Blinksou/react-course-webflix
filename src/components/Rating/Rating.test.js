import { render, screen } from "@testing-library/react";
import Rating from "./Rating";

describe(Rating.name, () => {
  it("renders component successfully", () => {
    render(<Rating note={1} />);
    const element = screen.getByTestId("Rating");
    expect(element).toBeInTheDocument();
  });
});
