function Rating({ note }) {
  let convertedRating = (note * 5) / 10;

  return (
    <div data-testid={"Rating"}>
      <h2>Note du public</h2>
      {Array.from({ length: 5 }).map((value, index) => (
        <span
          key={index}
          style={
            index < Math.floor(convertedRating)
              ? {}
              : { filter: "grayscale(1)" }
          }
        >
          ⭐
        </span>
      ))}
      {convertedRating} / 5
    </div>
  );
}

export default Rating;
