---
to: <%= absPath %>/<%= component_name %>.jsx
---
import useStyles from './<%= component_name %>.style';


function <%= component_name %>() {
  const classes = useStyles()

  return <div className={classes.root} data-testid="<%= component_name %>" />;
};

export default <%= component_name %>;