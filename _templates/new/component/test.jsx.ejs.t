---
to: <%= absPath %>/<%= component_name %>.test.jsx
---
import React from 'react';
import { render, screen } from '@testing-library/react';
import <%= component_name %> from './<%= component_name %>';

describe(<%= component_name %>.name, () => {
    it('renders component successfully', () => {
      render(<<%= component_name %>  />);
      const element = screen.getByTestId("<%= component_name %>");
      expect(element).toBeInTheDocument();
    });
});