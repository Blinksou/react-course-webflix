module.exports = {
  prompt: ({ inquirer }) => {
    const questions = [
      {
        type: "select",
        name: "category",
        message: "Choose folder",
        choices: ["components", "pages"],
      },
      {
        type: "input",
        name: "component_name",
        message: "What is the component name?",
      },
    ];
    return inquirer.prompt(questions).then((answers) => {
      const { category, component_name } = answers;
      const path = `${category}/${component_name}`;
      const absPath = `src/${path}`;
      return { ...answers, path, absPath, category };
    });
  },
};
