---
to: <%= absPath %>/<%= component_name %>.style.js
---
import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  root: {},
});

export default useStyles;
